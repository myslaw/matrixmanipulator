#pragma once
#include <iterator>

template<typename T>
class MatrixIterator{ 
private:
	T* currentPointer;
	int colIdx;
	const int endColIdx;
	const int interRowStep;
public:
	using iterator_category = std::bidirectional_iterator_tag; //TODO change to std::random_access_iterator_tag
	using value_type = T;
	using pointer = T*;
	using reference = T&;
	MatrixIterator(T* currentPointer, int interRowStep, int relativeEndColumnIndex);
	~MatrixIterator() = default;
public:
	reference operator*() {
		return *currentPointer;
	}
	pointer operator->() {
		return currentPointer;
	}
	MatrixIterator<T>& operator++() {
		++colIdx;
		if (colIdx > endColIdx) {
			colIdx = 0;
			currentPointer += interRowStep;
		}
		else {
			++currentPointer;
		}
		return *this;
	}
	MatrixIterator<T> operator++(int) {
		MatrixIterator<T> temp = *this;
		++(*this);
		return temp;
	}
	MatrixIterator<T>& operator--() {
		--colIdx;
		if (colIdx < 0) {
			colIdx = endColIdx;
			currentPointer -= interRowStep;
		}
		else {
			--currentPointer;
		}
		return *this;
	}
	MatrixIterator<T> operator--(int) {
		MatrixIterator<T> temp = *this;
		--(*this);
		return temp;
	}

	friend bool operator== (const MatrixIterator<T>& a, const MatrixIterator<T>& b) {
		return a.currentPointer == b.currentPointer;
	}
	friend bool operator!= (const MatrixIterator<T>& a, const MatrixIterator<T>& b) {
		return a.currentPointer != b.currentPointer;
	}
};

template<typename T>
inline MatrixIterator<T>::MatrixIterator(T* currentPointer, int interRowStep,  int relativeEndColumnIndex) : currentPointer(currentPointer), interRowStep(interRowStep), endColIdx(relativeEndColumnIndex), colIdx(0)
{
}