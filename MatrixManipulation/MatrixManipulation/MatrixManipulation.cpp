#include <iostream>
#include <algorithm>
#include "Matrix.hpp"


Matrix<int> returnObj() {
    Matrix<int> test(2, 2);
    test.toString();
    return test;
}

template<typename T>
std::shared_ptr<T[]> createArray(int size) {
    std::shared_ptr<T[]> sp(new T[size]);
    for (int i = 0; i < size; ++i) {
        sp[i] = i;
    }
    return sp;
}

std::shared_ptr<int[]> createIntArray(int size) {
    return createArray<int>(size);
}




int additionTest() {
    std::cout << "Addition test:\n";
    
    auto dataA = createIntArray(4);
    Matrix<int> matrixA(2, 2, dataA);

    auto dataB = createIntArray(4);
    Matrix<int> matrixB(2, 2, dataB);

    try {
        Matrix<int> matrixC = matrixA+matrixB;
        std::cout << matrixC.toString() << std::endl;
    }
    catch (std::invalid_argument& e) {
        std::cerr << e.what() << std::endl;
    }
    return 0;
}

int eyeTest() {
    std::cout << "Eye text:\n";
    Matrix<int> eyeMatrix = Matrix<int>::eye(2, 2);
    std::cout << eyeMatrix.toString() << std::endl;
    return 0;
}

int onesTest() {
    std::cout << "Ones test:\n";
    Matrix<int> onesMatrix = Matrix<int>::ones(2, 2);
    std::cout << onesMatrix.toString() << std::endl;
    return 0;
}

int zerosTest() {
    std::cout << "Zeros test:\n";
    Matrix<int> zerosMatrix = Matrix<int>::zeros(2, 2);
    std::cout << zerosMatrix.toString() << std::endl;
    return 0;
}

int iteratorTest() {
    std::cout << "Iterator test:\n";
    Matrix<int> eyeMatrix = Matrix<int>::eye(2, 2);
    for (auto it = eyeMatrix.begin(); it != eyeMatrix.end(); ++it) {
        std::cout << *it << "\t";
    }
    std::cout << std::endl;
    return 0;
}

int submatrixTest() {
    std::cout << "Submatrix test:\n";
    Matrix<int> eyeMatrix = Matrix<int>::eye(4, 4);
    Matrix<int> subEyeMatrix = eyeMatrix(0, 2, 0, 4);
    std::cout << subEyeMatrix.toString() << std::endl;
    return 0;
}

int randomAccessTest() {
    std::cout << "Random access test:\n";
    Matrix<int> eyeMatrix = Matrix<int>::eye(4, 4);
    std::cout << std::to_string(eyeMatrix.at(1, 0)) << std::endl;
    return 0;
}

int valueChangeTest() {
    std::cout << "Value change test:\n";
    Matrix<int> eyeMatrix = Matrix<int>::eye(4, 4);
    eyeMatrix.at(1, 0) = 5;
    std::cout << eyeMatrix.toString() << std::endl;
    return 0;
}

int substractionTest() {
    std::cout << "Substraction test:\n";

    auto dataA = createIntArray(4);
    Matrix<int> matrixA(2, 2, dataA);

    auto dataB = createIntArray(4);
    Matrix<int> matrixB(2, 2, dataB);

    try {
        Matrix<int> matrixC = matrixA - matrixB;
        std::cout << matrixC.toString() << std::endl;
    }
    catch (std::invalid_argument& e) {
        std::cerr << e.what() << std::endl;
    }
    return 0;
}

int additionAssignmentTest() {
    std::cout << "Addition assignment test:\n";

    auto dataA = createIntArray(4);
    Matrix<int> matrixA(2, 2, dataA);

    auto dataB = createIntArray(4);
    Matrix<int> matrixB(2, 2, dataB);

    try {
        matrixA += matrixB;
        std::cout << matrixA.toString() << std::endl;
    }
    catch (std::invalid_argument& e) {
        std::cerr << e.what() << std::endl;
    }
    return 0;
}

int substractionAssignmentTest() {
    std::cout << "Subsctraction assignment test:\n";

    auto dataA = createIntArray(4);
    Matrix<int> matrixA(2, 2, dataA);

    auto dataB = createIntArray(4);
    Matrix<int> matrixB(2, 2, dataB);

    try {
        matrixA -= matrixB;
        std::cout << matrixA.toString() << std::endl;
    }
    catch (std::invalid_argument& e) {
        std::cerr << e.what() << std::endl;
    }
    return 0;
}

int scalarAdditionTest() {
    std::cout << "Scalar addition test:\n";

    auto dataA = createIntArray(4);
    Matrix<int> matrixA(2, 2, dataA);

    matrixA += 4;

    std::cout << matrixA.toString() << std::endl;
    return 0;
}

int scalarMultiplicationTest() {
    std::cout << "Scalar multiplication test:\n";

    auto dataA = createIntArray(5*5);
    Matrix<int> matrixA(5, 5, dataA);
    matrixA *= 15;
    std::cout << matrixA.toString() << std::endl;
    return 0;
}

int matrixMultiplicationTest() {
    std::cout << "Matrix multiplication test:\n";

    int x = 3;
    int y = 2;
    auto dataA = createIntArray(x * y);
    Matrix<int> matrixA(x, y, dataA);
    
    auto dataB = createIntArray(y * x);
    Matrix<int> matrixB(y, x, dataB);
    std::cout << "A:" << matrixA.toString() << "\nB:" << matrixB.toString() << '\n';


    auto matrixC = matrixA * matrixB;
    std::cout << "C:" << matrixC.toString() << std::endl;
    return 0;
}

int matrixDeterminantTest() {
    std::cout << "Matrix determinant test:\n";

    Matrix<int> matrixA(3, 3, createIntArray(3 * 3));
    matrixA.at(0, 0) = 1;

    std::cout << "A:" << matrixA.toString() << '\n' << "DET:" << matrixA.det() << std::endl;
    return 0;
}

int matrixScalarMultiplicationAsNewMatrix() {
    std::cout << "Matrix multiplication by scalara and return new matrix test:\n";

    int x = 3;
    int y = 2;
    auto dataA = createIntArray(x * y);
    Matrix<int> matrixA(x, y, dataA);
    Matrix<int> matrixB = matrixA * 2;

    std::cout << "A:" << matrixB.toString() << '\n';
    return 0;
}

int matrixInversionTest() {
    std::cout << "Matrix inversion test:\n";

    Matrix<double> matrixA(3, 3, createArray<double>(3 * 3));
    matrixA.at(0, 0) = 1;
    std::cout << "INV:" << matrixA.inv().toString() << std::endl;
    return 0;
}

int matrixTranspositionTest() {
    std::cout << "Matrix transposition test:\n";

    Matrix<int> matrixA(5, 2, createArray<int>(5 * 2));
    std::cout << "A:" << matrixA.toString() << '\n' << "T:" << matrixA.t().toString() << std::endl;
    return 0;
}

int matrixReshapeTest() {
    std::cout << "Matrix reshape test:\n";

    Matrix<int> matrixA(5, 2, createArray<int>(5 * 2));
    std::cout << "A:" << matrixA.toString() << '\n';
    matrixA.reshape(1, 10);
    std::cout << "R:" << matrixA.toString() << std::endl;
    return 0;
}

int matrixHorizontalConcatTest() {
    std::cout << "Matrix horizontal concatenation test:\n";

    Matrix<int> matrixA(2, 3, createArray<int>(2 * 3));
    Matrix<int> matrixB(2, 2, createArray<int>(2 * 2));
    matrixA.reshape(1, 6);
    matrixB.reshape(1, 4);
    std::cout << "A:" << matrixA.toString() << '\n' << "B:" << matrixB.toString() << '\n';
    Matrix<int> matrixC = Matrix<int>::hconcat({matrixA, matrixB});
    std::cout << "C:" << matrixC.toString() << std::endl;
    return 0;
}

int matrixVerticalConcatTest() {
    std::cout << "Matrix vertical concatenation test:\n";

    Matrix<int> matrixA(3, 2, createArray<int>(3 * 2));
    Matrix<int> matrixB(2, 2, createArray<int>(2 * 2));
    std::cout << "A:" << matrixA.toString() << '\n' << "B:" << matrixB.toString() << '\n';
    Matrix<int> matrixC = Matrix<int>::vconcat({ matrixA, matrixB });
    std::cout << "C:" << matrixC.toString() << std::endl;
    return 0;
}

int main()
{
    eyeTest();
    onesTest();
    zerosTest();
    additionTest();
    iteratorTest();
    submatrixTest();
    randomAccessTest();
    valueChangeTest();
    substractionTest();
    additionAssignmentTest();
    substractionAssignmentTest();
    scalarAdditionTest();
    scalarMultiplicationTest();
    matrixMultiplicationTest();
    matrixDeterminantTest();
    matrixScalarMultiplicationAsNewMatrix();
    matrixInversionTest();
    matrixTranspositionTest();
    matrixReshapeTest();
    matrixHorizontalConcatTest();
    matrixVerticalConcatTest();
}
