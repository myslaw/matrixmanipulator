#pragma once
#include <string>
#include <initializer_list>
#include "MatrixIterator.hpp"

template<typename T>
class Matrix {
private:
	int rows;
	int cols;
	std::shared_ptr<T[]> contents;
	int begColOffset;
	int begRowOffset;
	int endColOffset;
	int endRowOffset;
	int rowSpan;
	int colSpan;
	T* originPointer;
	int interRowStep;
	bool reshaped;
	int reshapedRowSpan;
	int reshapedColSpan;
public:
	static Matrix<T> eye(int targetRows, int cotargetCols);
	static Matrix<T> ones(int targetRows, int targetCols);
	static Matrix<T> zeros(int targetRows, int targetCols);
	static Matrix<T> filled(int targetRows, int targetCols, T val);
	static Matrix<T> hconcat(const std::initializer_list<Matrix<T>>& matrices);
	static Matrix<T> vconcat(const std::initializer_list<Matrix<T>>& matrices);
public:
	Matrix() = default;
	Matrix(const Matrix<T>& m);
	Matrix(int rows, int cols);
	Matrix(int rows, int cols, std::shared_ptr<T[]> data);
	~Matrix() = default;
public:
	std::string toString();
	MatrixIterator<T> begin();
	MatrixIterator<T> end();
	Matrix<T> operator+(const Matrix<T>& other);
	Matrix<T>& operator+=(const Matrix<T>& other);
	Matrix<T>& operator+=(const T& other);
	Matrix<T> operator-(const Matrix<T>& other);
	Matrix<T>& operator-=(const Matrix<T>& other);
	Matrix<T>& operator-=(const T& other);
	Matrix<T> operator*(const Matrix<T>& other);
	Matrix<T> operator*(const T& scalar);
	Matrix<T>& operator*=(const T& scalar);
	Matrix<T>& operator=(const Matrix<T>& other);
	Matrix<T> operator()(int begRowIdx, int rowCount, int begColIdx, int colCount);
	T& at(int row, int col);
	const T& at(int row, int col) const;
	T det() const;
	Matrix<T> adj() const;
	Matrix<T> inv() const;
	void fill(const T& val);
	Matrix<T> t() const;
	Matrix<T> clone() const;
	void reshape(const int targetRows, const int targetCols);
	void create(const int targetRows, const int targetCols);
	int getRowCount() const;
	int getColCount() const;
private:
	Matrix(int rows, int cols, std::shared_ptr<T[]> data, int begRow, int rowCount, int begCol, int colCount);
	T calcDeterminant(const T* matrix, const int n) const;
	void getCofactor(const T* mat, T* temp, const int p, const int q, const int n) const;
	void getAdjoint(const T* matrixA, T* matrixAdj) const;
	void calcInverse(const T* matrixA, T* matrixInverse) const;
};

template<typename T>
Matrix<T>::Matrix(const Matrix<T>& m)
{
	std::cout << "KONSTRUKTOR KOPIUJACY, SPRAWDZ KOD I NAPRAW!" << std::endl;
	rows = m.rows;
	cols = m.cols;
	contents = m.contents;
	begColOffset = m.begColOffset;
	begRowOffset = m.begRowOffset;
	endColOffset = m.endColOffset;
	endRowOffset = m.endRowOffset;
	rowSpan = m.rowSpan;
	colSpan = m.colSpan;
	originPointer = m.originPointer;
	interRowStep = m.interRowStep;
	reshaped = m.reshaped;
	reshapedRowSpan = m.reshapedRowSpan;
	reshapedColSpan = m.reshapedColSpan;
	//TODO complete me
}

template<typename T>
inline Matrix<T>::Matrix(int rows, int cols) : Matrix<T>(rows, cols, std::shared_ptr<T[]>(new T[rows*cols]))
{
}

template<typename T>
inline Matrix<T>::Matrix(int rows, int cols, std::shared_ptr<T[]> data) : rows(rows), cols(cols), contents(data), rowSpan(rows), colSpan(cols), originPointer(data.get()), interRowStep(1), begRowOffset(0), endRowOffset(0), begColOffset(0), endColOffset(0), reshaped(false), reshapedRowSpan(rows), reshapedColSpan(cols)
{
}

template<typename T>
void Matrix<T>::fill(const T& val)
{
	for (auto it = begin(); it != end(); ++it) {
		*it = val;
	}
}

template<typename T>
Matrix<T> Matrix<T>::t() const
{
	std::shared_ptr<T[]> transposedData(new T[colSpan * rowSpan]);
	for (int i = 0; i < rowSpan; ++i) {
		for (int j = 0; j < colSpan; ++j) {
			transposedData[j*rowSpan + i] = at(i, j);
		}
	}
	return Matrix<T>(colSpan, rowSpan, transposedData); //dimensions reversed on purpose! (because of transposition)
}

template<typename T>
inline Matrix<T> Matrix<T>::clone() const
{
	std::shared_ptr<T[]> dataCopy(new T[rowSpan * colSpan]);
	for (int i = 0; i < rowSpan; ++i) {
		for (int j = 0; j < colSpan; ++j) {
			dataCopy[i * rowSpan + j] = at(i, j);
		}
	}
	return Matrix<T>(rowSpan, colSpan, dataCopy);
}

template<typename T>
inline void Matrix<T>::reshape(const int targetRows, const int targetCols)
{
	if (targetRows * targetCols != rowSpan * colSpan) {
		throw std::invalid_argument("Reshaped matrix overall size cannot be changed");
	}
	reshapedRowSpan = targetRows;
	reshapedColSpan = targetCols;
	if (targetRows == rowSpan && targetCols == colSpan) {
		reshaped = false;
		return;
	}
	reshaped = true;
}

template<typename T>
inline void Matrix<T>::create(const int targetRows, const int targetCols)
{
}

template<typename T>
inline int Matrix<T>::getRowCount() const
{
	if (reshaped) {
		return reshapedRowSpan;
	}
	return rowSpan;
}

template<typename T>
inline int Matrix<T>::getColCount() const
{
	if (reshaped) {
		return reshapedColSpan;
	}
	return colSpan;
}

template<typename T>
inline Matrix<T>::Matrix(int rows, int cols, std::shared_ptr<T[]> data, int begRowIdx, int rowCount, int begColIdx, int colCount) : Matrix<T>(rows, cols, data)
{
	begRowOffset = begRowIdx;
	endRowOffset = rows - (begRowOffset + rowCount);
	rowSpan = rowCount;
	begColOffset = begColIdx;
	endColOffset = cols - (begColOffset + colCount);
	colSpan = colCount;
	originPointer += begRowOffset * cols; //move origin by n rows
	originPointer += begColOffset; //move origin by n cols;
	interRowStep = begColOffset + endColOffset + 1;

}

template<typename T>
T Matrix<T>::calcDeterminant(const T* matrix, const int n) const
{
	if (n == 1) {
		return matrix[0];
	}
	T result = T();
	T* temp = new T[n * n];
	int sign = 1;
	for (int f = 0; f < n; ++f) {
		getCofactor(matrix, temp, 0, f, n);		
		result += sign * matrix[f] * calcDeterminant(temp, n - 1);
		sign = -sign;
	}
	delete[] temp;
	return result;
}

template<typename T>
void Matrix<T>::getCofactor(const T* mat, T* temp, const int p,
	const int q, const int n) const
{
	int i = 0, j = 0;

	for (int row = 0; row < n; row++)
	{
		for (int col = 0; col < n; col++)
		{
			if (row != p && col != q)
			{
				temp[i * n + j] = mat[row * colSpan + col];
				++j;
				if (j == n - 1)
				{
					j = 0;
					i++;
				}
			}
		}
	}
}

template<typename T>
void Matrix<T>::getAdjoint(const T* matrixA, T* matrixAdj) const
{
	int N = rowSpan; //can be colSpan, since the matrix is square
	if (N == 1) {
		matrixAdj[0] = 1;
		return;
	}

	int sign = 1;
	T* temp = new T[N * N];
	for (int i = 0; i < N; ++i) {
		for (int j = 0; j < N; ++j) {
			getCofactor(matrixA, temp, i, j, N);
			// sign of adj[j][i] positive if sum of row
			// and column indexes is even.
			sign = ((i + j) % 2 == 0) ? 1 : -1;

			// Interchanging rows and columns to get the
			// transpose of the cofactor matrix
			matrixAdj[j * N + i] = (sign) * (calcDeterminant(temp, N - 1));

		}
	}
	delete[] temp;

}

template<typename T>
void Matrix<T>::calcInverse(const T* matrixA, T* matrixInverse) const
{
	int N = rowSpan; //square matrix so rowSpan = colSpan
	T detVal = calcDeterminant(matrixA, N);
	if (detVal == 0) {
		throw std::invalid_argument("Used matrix is singular, cannot find its inverse");
	}

	T* adjointMatrix = new T[N * N];
	getAdjoint(matrixA, adjointMatrix);
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++) {
			matrixInverse[i * N + j] = adjointMatrix[i * N + j] * (1.0 / detVal);
		}
	}
	delete[] adjointMatrix;
	
}


template<typename T>
Matrix<T> Matrix<T>::eye(int targetRows, int targetCols)
{
	std::shared_ptr<T[]> eyeData(new T[targetRows * targetCols]);
	for (int i = 0; i < targetRows; ++i) {
		for (int j = 0; j < targetCols; ++j) {
			eyeData[i * targetCols + j] = T();
			if (i == j) {
				eyeData[i * targetCols + j] += 1;
			}
		}
	}
	return Matrix<T>(targetRows, targetCols, eyeData);
}

template<typename T>
Matrix<T> Matrix<T>::ones(int targetRows, int targetCols)
{
	std::shared_ptr<T[]> onesData(new T[targetRows * targetCols]);
	for (int i = 0; i < targetRows; ++i) {
		for (int j = 0; j < targetCols; ++j) {
			onesData[i * targetCols + j] = T() + 1;
		}
	}
	return Matrix<T>(targetRows, targetCols, onesData);
}

template<typename T>
Matrix<T> Matrix<T>::zeros(int targetRows, int targetCols)
{
	std::shared_ptr<T[]> zerosData(new T[targetRows * targetCols]);
	for (int i = 0; i < targetRows; ++i) {
		for (int j = 0; j < targetCols; ++j) {
			zerosData[i * targetCols + j] = T();
		}
	}
	return Matrix<T>(targetRows, targetCols, zerosData);
}

template<typename T>
Matrix<T> Matrix<T>::filled(int targetRows, int targetCols, T val)
{
	std::shared_ptr<T[]> zerosData(new T[targetRows * targetCols]);
	for (int i = 0; i < targetRows; ++i) {
		for (int j = 0; j < targetCols; ++j) {
			zerosData[i * targetCols + j] = val;
		}
	}
	return Matrix<T>(targetRows, targetCols, zerosData);
}

template<typename T>
Matrix<T> Matrix<T>::hconcat(const std::initializer_list<Matrix<T>>& matrices)
{
	//concat side by side (same number of rows)
	int totalColSpan = 0;

	for (const Matrix<T>& m : matrices) {
		totalColSpan += m.getColCount();
	}

	int rowSpan = matrices.begin()->getRowCount();
	std::shared_ptr<T[]> concatenatedData(new T[rowSpan * totalColSpan]);
	
	int prevMatrixColSpan = 0;
	for (const Matrix<T>& m : matrices) {
		for (int i = 0; i < rowSpan ; ++i) {
			for (int j = 0; j < m.getColCount(); ++j) {
				concatenatedData[i * totalColSpan + j + prevMatrixColSpan] = m.at(i, j);
			}
		}
		prevMatrixColSpan += m.getColCount();
	}
	return Matrix<T>(rowSpan, totalColSpan, concatenatedData);
}

template<typename T>
inline Matrix<T> Matrix<T>::vconcat(const std::initializer_list<Matrix<T>>& matrices)
{
	//concat one over another (same number of cols)
	int totalRowSpan = 0;
	for (const Matrix<T>& m : matrices) {
		totalRowSpan += m.getRowCount();
	}

	int colSpan = matrices.begin()->getColCount();
	std::shared_ptr<T[]> concatenatedData(new T[totalRowSpan * colSpan]);

	int prevMatrixRowSpan = 0;
	for (const Matrix<T>& m : matrices) {
		for (int i = 0; i < m.getRowCount() ; ++i) {
			for (int j = 0; j < colSpan ; ++j) {
				concatenatedData[(i + prevMatrixRowSpan) * colSpan + j] = m.at(i, j);
			}
		}
		prevMatrixRowSpan += m.getRowCount();
	}
	return Matrix<T>(totalRowSpan, colSpan, concatenatedData);
}

template<typename T>
std::string Matrix<T>::toString()
{
	std::string matrixAsString;
	matrixAsString.reserve(rowSpan * colSpan * 2 + 1); //efficient if 1 char per cell
	matrixAsString += '[';

	if (reshaped) {
		for (int i = 0; i < reshapedRowSpan; ++i) {
			for (int j = 0; j < reshapedColSpan; ++j) {
				matrixAsString += std::to_string(at(i, j));
				matrixAsString += ',';
			}
			matrixAsString.pop_back();
			matrixAsString += ';';
		}
	}
	else {
		for (int i = 0; i < rowSpan; ++i) {
			for (int j = 0; j < colSpan; ++j) {
				matrixAsString += std::to_string(at(i, j));
				matrixAsString += ',';
			}
			matrixAsString.pop_back();
			matrixAsString += ';';
		}
	}
	matrixAsString.pop_back();
	matrixAsString += ']';
	return matrixAsString;
}

template<typename T>
MatrixIterator<T> Matrix<T>::begin()
{
	int relativeEndColumnIndex = cols - interRowStep;
	return MatrixIterator<T>(originPointer, interRowStep, relativeEndColumnIndex);
}

template<typename T>
MatrixIterator<T> Matrix<T>::end()
{
	T* destinationPointer = contents.get();
	destinationPointer += (rows - endRowOffset) * cols; //move destination to last relative row
	destinationPointer += begColOffset; //move destination to first relative column
	int relativeEndColumnIndex = cols - interRowStep;
	return MatrixIterator<T>(destinationPointer, interRowStep, relativeEndColumnIndex);
}

template<typename T>
Matrix<T> Matrix<T>::operator+(const Matrix<T>& other)
{
	if (rowSpan != other.rowSpan || colSpan != other.colSpan) {
		throw std::invalid_argument("Matrices shape/size does not match");
	}
	int size = colSpan * rowSpan;
	std::shared_ptr<T[]> resultData(new T[size]);
	for (int i = 0; i < rowSpan; ++i) {
		for (int j = 0; j < colSpan; ++j) {
			resultData[i * colSpan + j] = at(i, j) + other.at(i, j);
		}
	}
	return Matrix<T>(rowSpan, colSpan, resultData);
}

template<typename T>
Matrix<T>& Matrix<T>::operator+=(const Matrix<T>& other)
{
	if (rowSpan != other.rowSpan || colSpan != other.colSpan) {
		throw std::invalid_argument("Matrices shape/size does not match");
	}
	for (int i = 0; i < rowSpan; ++i) {
		for (int j = 0; j < colSpan; ++j) {
			at(i, j) += other.at(i, j);
		}
	}
	return *this;
}

template<typename T>
inline Matrix<T>& Matrix<T>::operator+=(const T& other)
{
	for (int i = 0; i < rowSpan; ++i) {
		for (int j = 0; j < colSpan; ++j) {
			at(i, j) += other;
		}
	}
	return *this;
}

template<typename T>
Matrix<T> Matrix<T>::operator-(const Matrix<T>& other)
{
	if (rowSpan != other.rowSpan || colSpan != other.colSpan) {
		throw std::invalid_argument("Matrices shape/size does not match");
	}
	int size = colSpan * rowSpan;
	std::shared_ptr<T[]> resultData(new T[size]);
	for (int i = 0; i < rowSpan; ++i) {
		for (int j = 0; j < colSpan; ++j) {
			resultData[i * colSpan + j] = at(i, j) - other.at(i, j);
		}
	}
	return Matrix<T>(rowSpan, colSpan, resultData);
}

template<typename T>
Matrix<T>& Matrix<T>::operator-=(const Matrix<T>& other)
{
	for (int i = 0; i < rowSpan; ++i) {
		for (int j = 0; j < colSpan; ++j) {
			at(i, j) -= other.at(i, j);
		}
	}
	return *this;
}

template<typename T>
Matrix<T>& Matrix<T>::operator-=(const T& other)
{
	for (int i = 0; i < rowSpan; ++i) {
		for (int j = 0; j < colSpan; ++j) {
			at(i, j) -= other;
		}
	}
	return *this;
}

template<typename T>
Matrix<T> Matrix<T>::operator*(const Matrix<T>& other)
{
	if (colSpan != other.rowSpan) {
		throw std::invalid_argument("First matrix columns count is not equal to second matrix rows count");
	}
	int size = rowSpan * other.colSpan;
	std::shared_ptr<T[]> result(new T[size]());
	for (int i = 0; i < rowSpan; ++i) {
		for (int j = 0; j < other.colSpan; ++j) {
			for (int k = 0; k < colSpan; ++k) {
				result[i * other.colSpan + j] += at(i, k) * other.at(k, j);
			}
		}
	}
	return Matrix<T>(rowSpan, other.colSpan, result);
}

template<typename T>
Matrix<T> Matrix<T>::operator*(const T& scalar)
{
	std::shared_ptr<T[]> resultData(new T[rowSpan * colSpan]);

	for (int i = 0; i < rowSpan; ++i) {
		for (int j = 0; j < colSpan; ++j) {
			resultData[i * colSpan + j] = at(i, j) * scalar;
		}
	}
	return Matrix<T>(rowSpan, colSpan, resultData);
}

template<typename T>
Matrix<T>& Matrix<T>::operator*=(const T& scalar)
{
	for (int i = 0; i < rowSpan; ++i) {
		for (int j = 0; j < colSpan; ++j) {
			at(i, j) *= scalar;
		}
	}
	return *this;
}

template<typename T>
Matrix<T>& Matrix<T>::operator=(const Matrix<T>& other)
{
	rows = other.rows;
	cols = other.cols;
	contents = other.contents;
	begColOffset = other.begColOffset;
	begRowOffset = other.begRowOffset;
	endColOffset = other.endColOffset;
	endRowOffset = other.endRowOffset;
	colSpan = other.colSpan;
	rowSpan = other.rowSpan;
	originPointer = other.originPointer;
	interRowStep = other.interRowStep;
	reshaped = other.reshaped;
	reshapedRowSpan = other.reshapedRowSpan;
	reshapedColSpan = other.reshapedColSpan;
	//TODO copy new variables
}

template<typename T>
Matrix<T> Matrix<T>::operator()(int begRowIdx, int rowCount, int begColIdx, int colCount)
{
	return Matrix<T>(rows, cols, contents, begRowIdx, rowCount, begColIdx, colCount);
}

template<typename T>
T& Matrix<T>::at(int targetRowIdx, int targetColIdx)
{
	T* targetPointer = originPointer;
	int trueRowIdx;
	int trueColIdx;
	if (reshaped) {
		int cellIdx = reshapedColSpan * targetRowIdx + targetColIdx;
		trueRowIdx = cellIdx / colSpan;
		trueColIdx = cellIdx % colSpan;
	}
	else {
		trueRowIdx = targetRowIdx;
		trueColIdx = targetColIdx;
	}
	targetPointer += (colSpan + endColOffset + begColOffset) * trueRowIdx; //move to targeted row
	targetPointer += trueColIdx; //move to targeted column
	return *targetPointer;
}

template<typename T>
const T& Matrix<T>::at(int targetRowIdx, int targetColIdx) const
{
	T* targetPointer = originPointer;
	targetPointer += (colSpan + endColOffset + begColOffset) * targetRowIdx; //move to targeted row
	targetPointer += targetColIdx; //move to targeted column
	return *targetPointer;
}

template<typename T>
T Matrix<T>::det() const
{
	if (rowSpan != colSpan) {
		throw std::invalid_argument("Determinant can be calculated only for square matrix");
	}
	return calcDeterminant(originPointer, rowSpan);
}

template<typename T>
inline Matrix<T> Matrix<T>::adj() const
{
	if (rowSpan != colSpan) {
		throw std::invalid_argument("Matrix is not square, can't find adjoint matrix");
	}
	std::shared_ptr<T[]> resultData(new T[rowSpan * colSpan]);
	getAdjoint(originPointer, resultData.get());
	return Matrix<T>(rowSpan, colSpan, resultData);
}

template<typename T>
Matrix<T> Matrix<T>::inv() const
{
	if (rowSpan != colSpan) {
		throw std::invalid_argument("Matrix is not square, can't find its inverse");
	}
	std::shared_ptr<T[]> inverseData(new T[rowSpan * colSpan]);
	calcInverse(originPointer, inverseData.get());

	return Matrix<T>(rowSpan, colSpan, inverseData);
}
